//
//  GameOverLayer.m
//  RuralDuck
//
//  Created by Admin on 05.06.14.
//  Copyright (c) 2014 Diploma. All rights reserved.
//

#import "GameOverLayer.h"

@interface GameOverLayer()

@property (nonatomic, retain) SKSpriteNode* retryButton;
@property (nonatomic, retain) SKLabelNode *scoresNode;
@end


@implementation GameOverLayer

- (id)initWithSize:(CGSize)size
{
    if(self = [super initWithSize:size])
    {
        SKSpriteNode* startGameText = [SKSpriteNode spriteNodeWithImageNamed:@"gameOverScreen"];
        startGameText.position = CGPointMake(size.width * 0.5f, size.height * 0.5f);
       // startGameText.zPosition = 1;
        CGSize fullSize = [UIScreen mainScreen].bounds.size;
        //startGameText.size = fullSize;
        [self addChild:startGameText];
        
        SKSpriteNode* retryButton = [SKSpriteNode spriteNodeWithImageNamed:@"replay"];
        retryButton.position = CGPointMake(size.width * 0.5f, size.height * 0.30f);
       // retryButton.zPosition = 2;
        [self addChild:retryButton];
        
        SKAction *retryButtonAction = [SKAction repeatActionForever:[SKAction sequence:@[[SKAction resizeByWidth:retryButton.size.width*0.5f height:retryButton.size.height*0.5f duration:0.8],[SKAction resizeByWidth:retryButton.size.width*-0.5f height:retryButton.size.height*-0.5f duration:0.8]]]];
        
        [retryButton runAction:retryButtonAction];
        
        [self setRetryButton:retryButton];
    }
    
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    if ([_retryButton containsPoint:location])
    {
        if([self.delegate respondsToSelector:@selector(gameOverLayer:tapRecognizedOnButton:)])
        {
            [self.delegate gameOverLayer:self tapRecognizedOnButton:GameOverLayerPlayButton];
        }
    }
}

@end

