//
//  FirstViewController.m
//  WorkRunner
//
//  Created by Женя Михайлова on 06.08.15.
//  Copyright (c) 2015 Mobigear. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()
{
    int initialWidth;
    int initialHeight;
}
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft] forKey:@"orientation"];
    
    initialWidth = self.welcomeWidth.constant;
    self.welcomeWidth.constant = initialWidth * 0.4;
    initialHeight = self.welcomeHeight.constant;
    self.welcomeHeight.constant = initialHeight * 0.4;
    [self.welcomeImageView setNeedsUpdateConstraints];
    
    self.playBtnCenterConstraint.constant += self.view.bounds.size.width + 200;
    [self.playButton setNeedsUpdateConstraints];
    [self.playButton.layer setCornerRadius:5];
    [self.welcomeImageView layoutIfNeeded];
    [self.playButton layoutIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIImage *driver = [UIImage animatedImageNamed:@"angrydriver" duration:0.35f];
    self.driverImageView.image = driver;
    
    UIImage *fire = [UIImage animatedImageNamed:@"fire" duration:0.5f];
    self.fireImageView.image = fire;
        
    UIImage *granImage = [UIImage animatedImageNamed:@"granmove" duration:0.35f];
    self.grannyImageView.image = granImage;
    
    UIImage *sayImage = [UIImage animatedImageNamed:@"say" duration:0.3f];
    self.sayImageView.image = sayImage;
    
    UIImage *bottom = [UIImage animatedImageNamed:@"firstBack" duration:0.3f];
    self.bottomImageView.image = bottom;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.welcomeWidth.constant = initialWidth;
    self.welcomeHeight.constant = initialHeight;
    [self.welcomeImageView setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.welcomeImageView layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
    
    self.playBtnCenterConstraint.constant = 0;
    [self.playButton setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.4 animations:^{
        [self.playButton layoutIfNeeded];
    } completion:^(BOOL finished) {
        CABasicAnimation *animation =
        [CABasicAnimation animationWithKeyPath:@"position"];
        [animation setDuration:0.06];
        [animation setRepeatCount:1];
        [animation setAutoreverses:YES];
        [animation setFromValue:[NSValue valueWithCGPoint:
                                 CGPointMake([self.playButton center].x + 8.0f, [self.playButton center].y)]];
        [animation setToValue:[NSValue valueWithCGPoint:
                               CGPointMake([self.playButton center].x - 5.0f, [self.playButton center].y)]];
        [[self.playButton layer] addAnimation:animation forKey:@"position"];
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)playPressed:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        self.playButton.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            self.playButton.transform = CGAffineTransformMakeScale(1, 1);
        } completion:^(BOOL finished) {
            [self performSegueWithIdentifier:@"startGame" sender:nil];
        }];
    }];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
