//
//  GameOverLayer.h
//  RuralDuck
//
//  Created by Admin on 05.06.14.
//  Copyright (c) 2014 Diploma. All rights reserved.
//

#import "GameHelperLayer.h"

typedef NS_ENUM(NSUInteger, GameOverLayerButtonType)
{
    GameOverLayerPlayButton = 0
};

@protocol GameOverLayerDelegate;

@interface GameOverLayer : GameHelperLayer

@property (nonatomic, assign) id<GameOverLayerDelegate> delegate;
@end


@protocol GameOverLayerDelegate <NSObject>

@optional

- (void) gameOverLayer:(GameOverLayer*)sender tapRecognizedOnButton:(GameOverLayerButtonType) gameOverLayerButtonType;

@end

