//
//  AppDelegate.h
//  WorkRunner
//
//  Created by Faust on 05.08.15.
//  Copyright (c) 2015 Mobigear. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

