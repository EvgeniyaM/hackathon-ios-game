//
//  GameScene.h
//  WorkRunner
//

//  Copyright (c) 2015 Mobigear. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene <SKPhysicsContactDelegate>
@property UIViewController *parentController;
@end
