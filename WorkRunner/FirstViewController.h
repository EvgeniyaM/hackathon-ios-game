//
//  FirstViewController.h
//  WorkRunner
//
//  Created by Женя Михайлова on 06.08.15.
//  Copyright (c) 2015 Mobigear. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *bottomImageView;
//granny
@property (strong, nonatomic) IBOutlet UIImageView *grannyImageView;
// driver
@property (strong, nonatomic) IBOutlet UIImageView *driverImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sayImageView;
@property (strong, nonatomic) IBOutlet UIImageView *fireImageView;
// other
@property (strong, nonatomic) IBOutlet UIImageView *welcomeImageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *welcomeWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *welcomeHeight;
@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *welcomeCenterConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *playBtnCenterConstraint;

@end
