//
//  GameScene.m
//  WorkRunner
//
//  Created by Faust on 05.08.15.
//  Copyright (c) 2015 Mobigear. All rights reserved.
//

#import "GameScene.h"
#import "GameOverLayer.h"

@interface GameScene () <GameOverLayerDelegate>

@property SKTexture* textureGround;
@property SKTexture* textureSkyline;
@property SKTexture* textureSky;

@property (nonatomic) SKNode *physicalRunnerNode;
@property (nonatomic) SKNode *physicalFallingNode;
@property (nonatomic) SKNode *physicalGrannyNode;
@property (nonatomic) SKNode *physicalCopNode;

@property (nonatomic) SKSpriteNode *spriteRunnerNode;

@property (nonatomic) SKSpriteNode *spriteJumpNode;
@property (nonatomic) SKSpriteNode *spriteRightNode;
@property (nonatomic) SKSpriteNode *spriteLeftNode;
@property (nonatomic) SKSpriteNode *spriteForceMoveNode;
@property (nonatomic) SKSpriteNode *spriteFallingNode;
@property (nonatomic) SKSpriteNode *spriteGrannyNode;
@property (nonatomic) SKSpriteNode *spriteZebraNode;
@property (nonatomic) SKSpriteNode *spriteCopNode;

@property (nonatomic) SKAction *runnerMovingAction;
@property (nonatomic) SKAction *copMovingAction;
@property (nonatomic) SKAction *grannyMovingAction;
@property (nonatomic) SKAction *brickMovingAction;
@end

@implementation GameScene {
    SKColor* _skyColor;
    NSArray * _runnerMovingFrames;
    NSArray * _copMovingFrames;
    NSArray * _grannyMovingFrames;
    NSArray * _brickMovingFrames;
    BOOL _jumpActionTemp;
    BOOL _rightMoveActionTemp;
    BOOL _leftMoveActionTemp;
    int _fallingBlocksTemp;
    int _grannyTemp;
    int _copTemp;
    BOOL _secondCopTemp;
    int _totalScores;
    __block SKLabelNode * _scoresNode;
    
    SKTexture* _jumpBlockTexture;
    SKAction* _moveAndRemovejumpBlocks;
    
    SKTexture* _breakBlockTexture;
    SKNode* _breakBlocks;
    SKAction* _moveAndRemovebreakBlocks;
    
    SKNode* _jumpBlocks;
    SKNode* _moving;
    SKLabelNode * _grannyScoressNode;
    SKSpriteNode * _grannySpriteNode;
    
    BOOL _gameStarted;
    BOOL _gameOver;
    
    int _grannyScore;
    
    NSArray *_breakBlockImgNames;
    //StartGameLayer* _startGameLayer;
    GameOverLayer* _gameOverLayer;
}

//static const uint32_t runnerCategory = 1 << 0;
//static const uint32_t worldCategory = 1 << 1;
//static const uint32_t jumpBlockCategory = 1 << 2;
//static const uint32_t grannyCategory = 1 << 1;

static const uint32_t runnerCategory = 0x1 << 0;
static const uint32_t worldCategory = 0x1 << 6;
static const uint32_t jumpBlockCategory = 0x1 << 5;
static const uint32_t breakBlockCategory = 0x1 << 7;
static const uint32_t grannyCategory = 0x1 << 3;
static const uint32_t fallingCategory = 0x1 << 4;
static const uint32_t copCategory = 0x1 << 2;

-(void)didMoveToView:(SKView *)view {
    
    [self createAll];
}

- (void)createAll
{
    _breakBlockImgNames = @[@"cone", @"hydrant", @"sign", @"grass"];
    
    [self createPhysicalWorld];
    [self runAction:[SKAction repeatActionForever:[SKAction playSoundFileNamed:@"backGround.mp3" waitForCompletion:YES]]];
    
    [self createSpawnHelpNodes];
    
    [self createTemps];
    
    [self createAnimatedGroundAndSky];
    
    [self createPhysicalBodies];
    
    [self createAtlases];
    
    [self createActions];
    
    [self createRunner];
    
    [self createButtons];
    
    [self createJumpBlocks];
    
    [self createBreakBlocks];
    
    [self createEmmiters];
    
    [self createScoresNode];
    
    [self initializeGameOverLayer];
    
    [_gameOverLayer removeFromParent];
}

#pragma mark levelCreation

- (void) initializeGameOverLayer
{
    _gameOverLayer = [[GameOverLayer alloc] initWithSize:self.parentController.view.frame.size];
    _gameOverLayer.userInteractionEnabled = YES;
    _gameOverLayer.zPosition = 3;
    _gameOverLayer.delegate = self;
    
}

- (void)createScoresNode
{
    _totalScores = 0;
    _scoresNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    _scoresNode.text = [NSString stringWithFormat:@"Total scores: %d", _totalScores];
    _scoresNode.fontSize = 20;
    _scoresNode.fontColor = [SKColor blackColor];
    _scoresNode.zPosition = 7;
    _scoresNode.position = CGPointMake(self.size.width - 100, self.size.height * 0.95);
    [self addChild:_scoresNode];
    
    _grannyScoressNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    _grannyScoressNode.text = [NSString stringWithFormat:@"%d", _grannyScore];
    _grannyScoressNode.fontSize = 20;
    _grannyScoressNode.fontColor = [SKColor blackColor];
    _grannyScoressNode.zPosition = 7;
    _grannyScoressNode.position = CGPointMake(40, self.size.height * 0.91);
    [self addChild:_grannyScoressNode];
    
    _grannySpriteNode = [SKSpriteNode spriteNodeWithImageNamed:@"granny_min"];
    _grannySpriteNode.zPosition = 7;
    _grannySpriteNode.name = @"granny_min";
    _grannySpriteNode.position = CGPointMake(20, self.size.height * 0.94);
    [self addChild:_grannySpriteNode];
    
}

-(void)createWallet {
    
    SKSpriteNode *wallet = [SKSpriteNode spriteNodeWithImageNamed:@"wallet"];
    wallet.zPosition = 7;
    wallet.name = @"wallet";
    wallet.position = CGPointMake(self.size.width *0.5f, self.size.height * 0.7f);
    [self addChild:wallet];
    
    [wallet runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction moveToX:self.size.width * 0.05 duration:1.9f],[SKAction moveToX:self.size.width *0.95 duration:1.9f]]]]];
    
}


-(void)createCop {
    
    self.physicalCopNode = [SKNode node];
    self.physicalCopNode.name=@"physicalCopNode";
    
    self.physicalCopNode.zPosition = 7;
    self.physicalCopNode.position = CGPointMake(self.size.width * 0.05, self.size.height* 0.3f);
   
    self.spriteCopNode = [SKSpriteNode spriteNodeWithImageNamed:@"cop"];
    self.spriteCopNode.name=@"spriteCopNode";
    self.spriteCopNode.position=CGPointMake(0.0, 0.0);
    
    self.physicalCopNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.spriteCopNode.size];
    self.physicalCopNode.physicsBody.dynamic = NO;
    self.physicalCopNode.physicsBody.affectedByGravity = NO;
    self.physicalCopNode.physicsBody.categoryBitMask = copCategory;
    self.physicalCopNode.physicsBody.contactTestBitMask = runnerCategory ;
    self.physicalCopNode.physicsBody.collisionBitMask = runnerCategory;
    
    
    [self addChild:self.physicalCopNode];
    [self.physicalCopNode addChild:self.spriteCopNode];
    [self createWallet];
    [self.spriteCopNode runAction:[SKAction repeatActionForever: self.copMovingAction]];
   // [self.physicalCopNode runAction:[SKAction repeatActionForever:[SKAction moveByX:10 y:0 duration:0.2f]]];
}


-(void)createGranny {
    
    self.physicalGrannyNode = [SKNode node];
    self.physicalGrannyNode.name=@"physicalGrannyNode";
    self.physicalGrannyNode.zPosition = 7;
    
    float randomNum = ((float)rand() / RAND_MAX) * 1;
    
    self.physicalGrannyNode.position = CGPointMake(self.size.width * randomNum, self.size.height* 0.35f);
    
    
    self.spriteGrannyNode = [SKSpriteNode spriteNodeWithImageNamed:@"granny"];
    self.spriteGrannyNode.name=@"spriteGrannyNode";
    self.spriteGrannyNode.position=CGPointMake(0.0, 0.0);
    
    self.physicalGrannyNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.spriteGrannyNode.size];
    self.physicalGrannyNode.physicsBody.dynamic = NO;
    self.physicalGrannyNode.physicsBody.affectedByGravity = NO;
    
    self.physicalGrannyNode.physicsBody.categoryBitMask = grannyCategory;
    self.physicalGrannyNode.physicsBody.contactTestBitMask = runnerCategory;
    self.physicalGrannyNode.physicsBody.collisionBitMask = runnerCategory;
    
    [self addChild:self.physicalGrannyNode];
    [self.physicalGrannyNode addChild:self.spriteGrannyNode];
    [self.spriteGrannyNode runAction:[SKAction repeatActionForever: self.grannyMovingAction]];
    SKAction *moveUp = [SKAction moveToY:self.size.height* 0.3f - 10 duration:1.2f];
    [self.physicalGrannyNode runAction:[SKAction sequence:@[moveUp,[SKAction runBlock:^{
        [self.physicalGrannyNode removeFromParent];
    }]]]];

    
}

-(void)createFallingBlocks {
    
    self.physicalFallingNode = [SKNode node];
    self.physicalFallingNode.name=@"physicalFallingNode";
    self.physicalFallingNode.zPosition = 7;
    float randomNum = ((float)rand() / RAND_MAX) * 1;
    self.physicalFallingNode.position = CGPointMake(self.size.width * randomNum, self.size.height* 0.9);
    //self.physicalFallingNode.physicsBody.dynamic = NO;
    self.spriteFallingNode = [SKSpriteNode spriteNodeWithImageNamed:@"moveBrick1.png"];
    self.spriteFallingNode.name=@"spriteFallingNode";
    self.spriteFallingNode.position=CGPointMake(0.0, 0.0);
    
    self.physicalFallingNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.spriteFallingNode.size];
    self.physicalFallingNode.physicsBody.dynamic = NO;
    self.physicalFallingNode.physicsBody.affectedByGravity = NO;
    self.physicalFallingNode.physicsBody.categoryBitMask = fallingCategory;
    self.physicalFallingNode.physicsBody.contactTestBitMask = runnerCategory;
    self.physicalFallingNode.physicsBody.collisionBitMask = runnerCategory;
    [self addChild:self.physicalFallingNode];
    [self.physicalFallingNode addChild:self.spriteFallingNode];
    [self.spriteFallingNode runAction:[SKAction repeatActionForever: self.brickMovingAction]];
    SKAction *moveDown = [SKAction moveToY:self.size.height* 0.3f - 10 duration:1.8f];
    [self.physicalFallingNode runAction:[SKAction sequence:@[moveDown,[SKAction runBlock:^{
        [self.physicalFallingNode removeFromParent];
    }]]]];
    
}

-(void)createSpawnHelpNodes {
    
    _moving = [SKNode node];
    [self addChild:_moving];
    
    _jumpBlocks = [SKNode node];
    [_moving addChild:_jumpBlocks];
    
    _breakBlocks = [SKNode node];
    [_moving addChild:_breakBlocks];
    
}

-(void)createPhysicalWorld {
    
    SKNode *rightBorder = [SKNode node];
    rightBorder.position = CGPointMake( self.frame.size.width - 5, 200);
    rightBorder.zPosition = -10;
    rightBorder.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10, 400)];
    rightBorder.physicsBody.dynamic = NO;
    rightBorder.physicsBody.categoryBitMask = jumpBlockCategory;
    rightBorder.physicsBody.contactTestBitMask = runnerCategory;
    rightBorder.name = @"rightBorder";
    [self addChild:rightBorder];
    
    SKNode *leftBorder = [SKNode node];
    leftBorder.position = CGPointMake(5, 200);
    leftBorder.zPosition = -10;
    leftBorder.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10, 400)];
    leftBorder.physicsBody.dynamic = NO;
    leftBorder.physicsBody.categoryBitMask = jumpBlockCategory;
    leftBorder.physicsBody.contactTestBitMask = runnerCategory;
    leftBorder.name = @"leftBorder";
    [self addChild:leftBorder];
    
    self.physicsWorld.contactDelegate = self;
}

-(void)createTemps {
    _secondCopTemp = YES;
    _jumpActionTemp = YES;
    _grannyScore = 0;
    _fallingBlocksTemp = 0;
    _grannyTemp = 0;
    _copTemp = 0;
}

-(void)createJumpBlocks {
    _jumpBlockTexture = [SKTexture textureWithImageNamed:@"hole2"];
    _jumpBlockTexture.filteringMode = SKTextureFilteringNearest;
    
    CGFloat distanceToMove = self.frame.size.width + 4 * _jumpBlockTexture.size.width;
    SKAction* movePipes = [SKAction moveByX:-distanceToMove y:0 duration:0.01 * distanceToMove];
    
    SKAction* updateScores = [SKAction runBlock:^{
        _totalScores++;
        _scoresNode.text = [NSString stringWithFormat:@"Total scores: %d", _totalScores];
    }];

    SKAction* removePipes = [SKAction removeFromParent];
    
    _moveAndRemovejumpBlocks = [SKAction sequence:@[movePipes,updateScores,removePipes]];
    
    SKAction* spawn = [SKAction performSelector:@selector(spawnJumpBlocks) onTarget:self];
    SKAction* delay = [SKAction waitForDuration:9.0];
    SKAction* spawnThenDelay = [SKAction sequence:@[spawn, delay]];
    SKAction* spawnThenDelayForever = [SKAction repeatActionForever:spawnThenDelay];
    [self runAction:spawnThenDelayForever];

}

- (void)createBreakBlocks
{
    _breakBlockTexture = [SKTexture textureWithImageNamed:@"hydrant"];
    _breakBlockTexture.filteringMode = SKTextureFilteringNearest;
    
    CGFloat distanceToMove = self.frame.size.width + 2.5 * _breakBlockTexture.size.width;
    SKAction* moveBreakBlock = [SKAction moveByX:-distanceToMove y:0 duration:0.01 * distanceToMove];
    
    SKAction* updateScores = [SKAction runBlock:^{
        _scoresNode.text = [NSString stringWithFormat:@"Total scores: %d", _totalScores];
    }];
    
    SKAction* removeBreakBlock = [SKAction removeFromParent];
    
    _moveAndRemovebreakBlocks = [SKAction sequence:@[moveBreakBlock, updateScores,removeBreakBlock]];
    
    SKAction* spawn = [SKAction performSelector:@selector(spawnBreakBlocks) onTarget:self];
    SKAction* delay = [SKAction waitForDuration:7.0];
    SKAction* spawnThenDelay = [SKAction sequence:@[spawn, delay]];
    SKAction* spawnThenDelayForever = [SKAction repeatActionForever:spawnThenDelay];
    [self runAction:spawnThenDelayForever];

}

-(void)createAnimatedGroundAndSky {
    
    _skyColor = [SKColor colorWithRed:96.0/255.0 green:216.0/255.0 blue:238.0/255.0 alpha:1.0];
    [self setBackgroundColor:_skyColor];
    
    self.textureGround = [SKTexture textureWithImageNamed:@"Ground"];
    self.textureGround.filteringMode = SKTextureFilteringNearest;
    
    SKAction* moveGroundSprite = [SKAction moveByX:-self.textureGround.size.width*2 y:0 duration:0.02 * self.textureGround.size.width*2];
    SKAction* resetGroundSprite = [SKAction moveByX:self.textureGround.size.width*2 y:0 duration:0];
    SKAction* moveGroundSpritesForever = [SKAction repeatActionForever:[SKAction sequence:@[moveGroundSprite, resetGroundSprite]]];
    
    for( int i = 0; i < 2 + self.frame.size.width / ( self.textureGround.size.width * 2 ); ++i ) {
       
        SKSpriteNode* sprite = [SKSpriteNode spriteNodeWithTexture:self.textureGround];
        [sprite setXScale:2.0];
        sprite.zPosition = -20;
        sprite.position = CGPointMake(i * sprite.size.width, sprite.size.height / 2);
        [sprite runAction:moveGroundSpritesForever];
         [_moving addChild:sprite];
    }
    
    self.textureSkyline = [SKTexture textureWithImageNamed:@"0"];
    self.textureSkyline.filteringMode = SKTextureFilteringNearest;
    
    SKAction* moveSkylineSprite = [SKAction moveByX:-self.textureSkyline.size.width*2 y:0 duration:0.1 * self.textureSkyline.size.width*2];
    SKAction* resetSkylineSprite = [SKAction moveByX:self.textureSkyline.size.width*2 y:0 duration:0];
    SKAction* moveSkylineSpritesForever = [SKAction repeatActionForever:[SKAction sequence:@[moveSkylineSprite, resetSkylineSprite]]];
    
    for( int i = 0; i < 2 + self.frame.size.width / ( self.textureSkyline.size.width ); ++i ) {
        SKSpriteNode *sprite;
        if (i == 0) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"0"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 1) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"1"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 2) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"2"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 3) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"3"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 4) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"4"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 5) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"5"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 6) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"6"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 7) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"7"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 8) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"0"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 9) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"1"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 10) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"2"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 11) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"3"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 12) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"4"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 13) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"5"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 14) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"6"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 15) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"7"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 16) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"0"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 17) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"1"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 18) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"2"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 19) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"3"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 20) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"4"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 21) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"5"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 22) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"6"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 23) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"7"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 24) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"0"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 25) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"1"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 26) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"2"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 27) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"3"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 28) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"4"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 29) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"5"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 30) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"6"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 31) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"7"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
             sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 32) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"0"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 33) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"1"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 34) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"2"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 35) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"3"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 36) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"4"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 37) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"5"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        } else if (i == 38) {
            self.textureSkyline = [SKTexture textureWithImageNamed:@"6"];
            self.textureSkyline.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSkyline];
        }
        
        
      //  [sprite setXScale:2.0];
        sprite.zPosition = -20;
        sprite.position = CGPointMake(i * sprite.size.width, self.size.height *0.5 + 30);
        [sprite runAction:moveSkylineSpritesForever];
         [_moving addChild:sprite];
    }
    
    self.textureSky = [SKTexture textureWithImageNamed:@"sky0"];
    self.textureSky.filteringMode = SKTextureFilteringNearest;
    
    SKAction* moveSkySprite = [SKAction moveByX:-self.textureSky.size.width*2 y:0 duration:0.1 * self.textureSky.size.width*2];
    SKAction* resetSkySprite = [SKAction moveByX:self.textureSky.size.width*2 y:0 duration:0];
    SKAction* moveSkySpritesForever = [SKAction repeatActionForever:[SKAction sequence:@[moveSkySprite, resetSkySprite]]];
    
    for( int i = 0; i < 2 + self.frame.size.width / ( self.textureSky.size.width ); ++i ) {
        SKSpriteNode *sprite;
        if (i == 0) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky0"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 1) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky1"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 2) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky2"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 3) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky3"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 4) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky4"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 5) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky5"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 6) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky6"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 7) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky7"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 8) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky0"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 9) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky1"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 10) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky2"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 11) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky3"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 12) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky4"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 13) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky5"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 14) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky6"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 15) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky7"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 16) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky0"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 17) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky1"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 18) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky2"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 19) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky3"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 20) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky4"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 21) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky5"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 22) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky6"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 23) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky7"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 24) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky0"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 25) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky1"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 26) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky2"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 27) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky3"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 28) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky4"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 29) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky5"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 30) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky6"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 31) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky7"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 32) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky0"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 33) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky1"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 34) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky2"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 35) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky3"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 36) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky4"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 37) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky5"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        } else if (i == 38) {
            self.textureSky = [SKTexture textureWithImageNamed:@"sky6"];
            self.textureSky.filteringMode = SKTextureFilteringNearest;
            sprite = [SKSpriteNode spriteNodeWithTexture:self.textureSky];
        }
        
        
        //  [sprite setXScale:2.0];
        sprite.zPosition = -20;
        sprite.position = CGPointMake(i * sprite.size.width, self.size.height *0.95);
        [sprite runAction:moveSkySpritesForever];
        [_moving addChild:sprite];
    }

    
}

-(void)createButtons {
    
    self.spriteJumpNode = [SKSpriteNode spriteNodeWithImageNamed:@"buttonJump"];
    self.spriteJumpNode.name=@"spriteJump";
    self.spriteJumpNode.position = CGPointMake(self.size.width * 0.92f, self.size.height* 0.1f);
    self.spriteJumpNode.zPosition = 7;
    
    [self.spriteJumpNode setScale:1.1];
    
    [self addChild:self.spriteJumpNode];
    
    self.spriteLeftNode = [SKSpriteNode spriteNodeWithImageNamed:@"back"];
    self.spriteLeftNode.name=@"spriteLeft";
    self.spriteLeftNode.position = CGPointMake(40, self.size.height* 0.1f);
    self.spriteLeftNode.zPosition = 7;
    [self.spriteLeftNode setScale:1.1];
    
    [self addChild:self.spriteLeftNode];
    
    self.spriteRightNode = [SKSpriteNode spriteNodeWithImageNamed:@"forward"];
    self.spriteRightNode.name=@"spriteRight";
    self.spriteRightNode.position = CGPointMake(self.spriteLeftNode.position.x + self.spriteRightNode.size.width + 20, self.size.height* 0.1f);
    self.spriteRightNode.zPosition = 7;
    [self.spriteRightNode setScale:1.1];
    
    [self addChild:self.spriteRightNode];

    
//    self.spriteForceMoveNode = [SKSpriteNode spriteNodeWithImageNamed:@"buttonJump"];
//    self.spriteForceMoveNode.name=@"spriteLeft";
//    self.spriteForceMoveNode.position = CGPointMake(self.size.width * 0.9f, self.size.height* 0.6f);
//    self.spriteForceMoveNode.zPosition = 7;
//    
//    [self.spriteForceMoveNode setScale:0.9];
//    
//    [self addChild:self.spriteForceMoveNode];
    
}

-(void)createRunner
{
    self.physicalRunnerNode = [[SKNode alloc]init];
    self.physicalRunnerNode.name=@"physicalRunner";
    self.physicalRunnerNode.zPosition = 7;
    self.physicalRunnerNode.position = CGPointMake(self.size.width * 0.5f, self.size.height* 0.3f - 5);
    
    self.spriteRunnerNode = [SKSpriteNode spriteNodeWithImageNamed:@"player"];
    self.spriteRunnerNode.name=@"spriteRunner";
    self.spriteRunnerNode.position=CGPointMake(0.0, 0.0);
    
    self.physicalRunnerNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.spriteRunnerNode.size];
    
    self.physicalRunnerNode.physicsBody.dynamic = YES;
    self.physicalRunnerNode.physicsBody.affectedByGravity = NO;
    
    self.physicalRunnerNode.physicsBody.categoryBitMask = runnerCategory;
    self.physicalRunnerNode.physicsBody.collisionBitMask = jumpBlockCategory;
    self.physicalRunnerNode.physicsBody.contactTestBitMask = worldCategory | jumpBlockCategory | copCategory | fallingCategory | grannyCategory;
    
    [self addChild:self.physicalRunnerNode];
    [self.physicalRunnerNode addChild:self.spriteRunnerNode];
    
    [self.spriteRunnerNode runAction:[SKAction repeatActionForever: self.runnerMovingAction]];
    
}

-(void)createPhysicalBodies {
    
//    SKNode* physicalGroundNode = [SKNode node];
//    physicalGroundNode.name = @"ground";
//    physicalGroundNode.position = CGPointMake(0, 0);
//    physicalGroundNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, self.size.height* 0.3f - 10)];
//    physicalGroundNode.physicsBody.dynamic = NO;
//    [self addChild:physicalGroundNode];
    
}

-(void)createAtlases{
    
    NSMutableArray *moveBrickFrames = [NSMutableArray array];
    SKTextureAtlas *brickMovingingAnimatedAtlas = [SKTextureAtlas atlasNamed:@"brick"];
    unsigned long numImagesBrickMove = brickMovingingAnimatedAtlas.textureNames.count;
    for (int i = 1; i <= numImagesBrickMove; i++) {
        NSString *textureNameMove= [NSString stringWithFormat:@"moveBrick%d", i];
        SKTexture *tempBrickMove = [brickMovingingAnimatedAtlas textureNamed:textureNameMove];
        [moveBrickFrames addObject:tempBrickMove];
    }
    _brickMovingFrames = moveBrickFrames;
    
    NSMutableArray *moveGrannyFrames = [NSMutableArray array];
    SKTextureAtlas *grannyMovingingAnimatedAtlas = [SKTextureAtlas atlasNamed:@"granny"];
    unsigned long numImagesGrannyMove = grannyMovingingAnimatedAtlas.textureNames.count;
    for (int i = 1; i <= numImagesGrannyMove; i++) {
        NSString *textureNameMove= [NSString stringWithFormat:@"move%d", i];
        SKTexture *tempGrannyMove = [grannyMovingingAnimatedAtlas textureNamed:textureNameMove];
        [moveGrannyFrames addObject:tempGrannyMove];
    }
    _grannyMovingFrames = moveGrannyFrames;
    
    NSMutableArray *moveCopFrames = [NSMutableArray array];
    SKTextureAtlas *copMovingingAnimatedAtlas = [SKTextureAtlas atlasNamed:@"police"];
    unsigned long numImagesCopMove = copMovingingAnimatedAtlas.textureNames.count;
    for (int i = 1; i <= numImagesCopMove; i++) {
        NSString *textureNameMove= [NSString stringWithFormat:@"move%d", i];
        SKTexture *tempCopMove = [copMovingingAnimatedAtlas textureNamed:textureNameMove];
        [moveCopFrames addObject:tempCopMove];
    }
    _copMovingFrames = moveCopFrames;
    
    NSMutableArray *moveRunnerFrames = [NSMutableArray array];
    SKTextureAtlas *runnerMovingingAnimatedAtlas = [SKTextureAtlas atlasNamed:@"driverAngry2"];
    unsigned long numImagesRunnerMove = runnerMovingingAnimatedAtlas.textureNames.count;
    for (int i = 1; i <= numImagesRunnerMove; i++) {
        NSString *textureNameMove= [NSString stringWithFormat:@"move%d", i];
        SKTexture *tempBasketMove = [runnerMovingingAnimatedAtlas textureNamed:textureNameMove];
        [moveRunnerFrames addObject:tempBasketMove];
    }
    _runnerMovingFrames = moveRunnerFrames;
    
    
    
}

-(void)createActions {
    self.runnerMovingAction = [SKAction animateWithTextures:_runnerMovingFrames
                                               timePerFrame:0.1f
                                                     resize:NO
                                                    restore:YES];
    self.copMovingAction = [SKAction animateWithTextures:_copMovingFrames
                                               timePerFrame:0.1f
                                                     resize:NO
                                                    restore:YES];
    self.grannyMovingAction = [SKAction animateWithTextures:_grannyMovingFrames
                                            timePerFrame:0.1f
                                                  resize:NO
                                                 restore:YES];
    self.brickMovingAction = [SKAction animateWithTextures:_brickMovingFrames
                                               timePerFrame:0.1f
                                                     resize:NO
                                                    restore:YES];
}

-(void)createEmmiters {
    NSString *particleSnowPath = [[NSBundle mainBundle] pathForResource:@"MyParticle" ofType:@"sks"];
    SKEmitterNode *snowParticle = [NSKeyedUnarchiver unarchiveObjectWithFile:particleSnowPath];
    
    
    snowParticle.particlePosition = CGPointMake(self.size.width * 0.5, self.size.height * 0.95);
    snowParticle.particleBirthRate = 100;
    
    
    [self addChild:snowParticle];
}

- (void)updateGrannyScore
{
    _grannyScoressNode.text = [NSString stringWithFormat:@"%d", _grannyScore];
}

#pragma mark contactDelegate

- (void)didBeginContact:(SKPhysicsContact *)contact {
    
    NSLog(@"contact.bodyA.node.name %@",contact.bodyA.node.name);
    NSLog(@"contact.bodyB.node.name %@",contact.bodyB.node.name);
    
    
    if ([contact.bodyA.node.name isEqualToString:@"ground"] ) {
        
    } else if ([contact.bodyA.node.name isEqualToString:@"rightBorder"]){
        
    }else if ([contact.bodyA.node.name isEqualToString:@"leftBorder"]){
        
    }else if ([contact.bodyB.node.name isEqualToString:@"physicalFallingNode"]){
        [self.physicalFallingNode runAction:[SKAction playSoundFileNamed:@"FrostmourneHungers.mp3" waitForCompletion:YES]];
        [self.physicalFallingNode removeFromParent];
        [self stopGame];
    }else if ([contact.bodyA.node.name isEqualToString:@"physicalFallingNode"]){
        [self.physicalFallingNode runAction:[SKAction playSoundFileNamed:@"FrostmourneHungers.mp3" waitForCompletion:YES]];
        [self.physicalFallingNode removeFromParent];
        [self stopGame];
    }else if ([contact.bodyB.node.name isEqualToString:@"physicalCopNode"]){
        [self.physicalCopNode runAction:[SKAction playSoundFileNamed:@"FrostmourneHungers.mp3" waitForCompletion:YES]];
        [self.physicalCopNode removeFromParent];
        [self stopGame];
    }else if ([contact.bodyA.node.name isEqualToString:@"physicalCopNode"]){
        [self.physicalCopNode runAction:[SKAction playSoundFileNamed:@"FrostmourneHungers.mp3" waitForCompletion:YES]];
        [self.physicalCopNode removeFromParent];
        [self stopGame];
    }else if ([contact.bodyB.node.name isEqualToString:@"physicalGrannyNode"]){
        _grannyScore ++;
        if (_grannyScore == 1) {
            [self runAction:[SKAction playSoundFileNamed:@"1stblood.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 2) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_dominate.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 3) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_holy.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 4) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_mega.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 5) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_monster.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 6) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_rampage.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 7) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_ultra.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 8) {
            [self runAction:[SKAction playSoundFileNamed:@"unstop.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 9) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_wicked.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 10) {
            [self runAction:[SKAction playSoundFileNamed:@"backGround.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 11) {
            [self runAction:[SKAction playSoundFileNamed:@"GodLike.wav" waitForCompletion:YES]];
        } else if (_grannyScore == 12) {
            [self runAction:[SKAction playSoundFileNamed:@"Ownage.wav" waitForCompletion:YES]];
        } else {
            [self runAction:[SKAction playSoundFileNamed:@"GodLike.wav" waitForCompletion:YES]];
        }
        [self updateGrannyScore];
        [self.physicalGrannyNode removeAllActions];
        if (!self.physicalCopNode.parent) {
            [self createCop];
        }
        [self.physicalGrannyNode runAction:[SKAction sequence:@[[SKAction group:@[[SKAction moveToX:self.size.width duration:2.0f],[SKAction moveToY:self.size.height - 30 duration:2.0f] ,[SKAction rotateByAngle:40 duration:2.0f]]],[SKAction removeFromParent]]]];
    }else if ([contact.bodyA.node.name isEqualToString:@"physicalGrannyNode"]){
        _grannyScore ++;
        if (_grannyScore == 1) {
            [self runAction:[SKAction playSoundFileNamed:@"1stblood.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 2) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_dominate.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 3) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_holy.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 4) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_mega.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 5) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_monster.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 6) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_rampage.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 7) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_ultra.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 8) {
            [self runAction:[SKAction playSoundFileNamed:@"unstop.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 9) {
            [self runAction:[SKAction playSoundFileNamed:@"kill_wicked.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 10) {
            [self runAction:[SKAction playSoundFileNamed:@"backGround.mp3" waitForCompletion:YES]];
        } else if (_grannyScore == 11) {
            [self runAction:[SKAction playSoundFileNamed:@"GodLike.wav" waitForCompletion:YES]];
        } else if (_grannyScore == 12) {
            [self runAction:[SKAction playSoundFileNamed:@"Ownage.wav" waitForCompletion:YES]];
        } else {
            [self runAction:[SKAction playSoundFileNamed:@"GodLike.wav" waitForCompletion:YES]];
        }

        [self.physicalGrannyNode removeAllActions];
        if (!self.physicalCopNode.parent) {
            [self createCop];
        }
        [self.physicalGrannyNode runAction:[SKAction sequence:@[[SKAction group:@[[SKAction moveToX:self.size.width duration:2.0f],[SKAction moveToY:self.size.height - 30 duration:2.0f],[SKAction rotateByAngle:40 duration:2.0f]]],[SKAction removeFromParent]]]];

    }
    else if ([contact.bodyB.node.name isEqualToString:@"break"]){
        _totalScores++;
        SKAction *blink = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.1],
                                               [SKAction fadeInWithDuration:0.1]]];
        SKAction *blinkForTime = [SKAction repeatAction:blink count:4];
        [contact.bodyB.node runAction:[SKAction sequence: @[blinkForTime, [SKAction removeFromParent]]]];
    }
    else if ([contact.bodyB.node.name isEqualToString:@"hole"]){
        [self runAction:[SKAction playSoundFileNamed:@"FrostmourneHungers.mp3" waitForCompletion:YES]];
        [self stopGame];
    } else if ([contact.bodyB.node.name isEqualToString:@"pipe1"] || [contact.bodyA.node.name isEqualToString:@"pipe1"]){
        [self runAction:[SKAction playSoundFileNamed:@"FrostmourneHungers.mp3" waitForCompletion:YES]];
        [self stopGame];
    }
    
}

#pragma mark touches

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    NSArray *nodes = [self nodesAtPoint:[touch locationInNode:self]];
    for (SKNode *node in nodes) {
        if ([node.name isEqualToString:@"spriteJump"]) {
            if (_jumpActionTemp == YES && _rightMoveActionTemp == NO && _leftMoveActionTemp == NO) {
                [self createJumpAnimation];
            }
        } else if ([node.name isEqualToString:@"spriteRight"]) {
            _rightMoveActionTemp = YES;
            [self moveRunnerRight];
        } else if ([node.name isEqualToString:@"spriteLeft"]) {
            _leftMoveActionTemp = YES;
            [self moveRunnerLeft];
        } else if ([node.name isEqualToString:@"wallet"]) {
            [node removeAllActions];
            [node runAction:[SKAction group:@[[SKAction moveToX:self.physicalCopNode.position.x duration:1.5f],[SKAction moveToY:self.physicalCopNode.position.y duration:1.5f],[SKAction rotateByAngle:40 duration:1.5f],[SKAction scaleTo:0.2 duration:1.5f],[SKAction fadeAlphaTo:0 duration:1.5f],[SKAction runBlock:^{
                [self.physicalCopNode removeFromParent];
            }]]]];
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_rightMoveActionTemp == YES) {
        //[self.physicalRunnerNode.physicsBody applyImpulse:CGVectorMake(0, 0)];
        [self.physicalRunnerNode removeAllActions];
        _rightMoveActionTemp = NO;
    }
    
    if (_leftMoveActionTemp == YES) {
        [self.physicalRunnerNode removeAllActions];
        _leftMoveActionTemp = NO;
    }
    
}

#pragma mark playerAnimations

-(void)createJumpAnimation {
    _jumpActionTemp = NO;
   
    SKAction *jumpActionUp = [SKAction moveByX:70.0f y:100  duration:0.4];
    SKAction *jumpActionDown = [SKAction moveByX:70.0f y:-100 duration:0.4];
    SKAction *blockActionJump = [SKAction runBlock:^{
        _jumpActionTemp = YES;
    }];
    SKAction *jumpSumAction = [SKAction sequence:@[jumpActionUp,jumpActionDown,blockActionJump]];
    [self.physicalRunnerNode runAction:jumpSumAction];
}

-(void)moveRunnerLeft {
    SKAction *moveLeftAction = [SKAction moveByX:-20 y:0 duration:0.2];
    [self.physicalRunnerNode runAction:[SKAction repeatActionForever:moveLeftAction]];
}

-(void)moveRunnerRight {
    //SKAction *animationAction = [SKAction speedTo:1.2 duration:0.2];
    SKAction *moveRightAction = [SKAction moveByX:20 y:0 duration:0.2];
    //SKAction *speedUp = [SKAction sequence:@[moveRightAction, animationAction]];
    [self.physicalRunnerNode runAction:[SKAction repeatActionForever:moveRightAction]];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    _fallingBlocksTemp ++;
    _grannyTemp ++;
    _copTemp ++;
    
    if (_copTemp % 120 == 0 && self.physicalCopNode.parent && _secondCopTemp == YES) {
         _secondCopTemp = NO;
        [self.physicalCopNode runAction:[SKAction sequence:@[[SKAction moveToX:self.size.width *0.5f + 50 duration:2.5f],[SKAction moveToX:self.size.width *0.05 duration:2.5f],[SKAction runBlock:^{
            _secondCopTemp = YES;
        }]]]];
    }
    
    if (_fallingBlocksTemp % 100 == 0 && !self.physicalFallingNode.parent) {
        [self createFallingBlocks];
    }
    
    if (_grannyTemp % 20 == 0 && !self.physicalGrannyNode.parent) {
        [self createGranny];
    }
   
}

#pragma mark spawnMethods

-(void)spawnJumpBlocks{
    SKNode* pipePair = [SKNode node];
    pipePair.position = CGPointMake( self.frame.size.width + _jumpBlockTexture.size.width * 2, self.size.height *0.3f - 38);
    pipePair.zPosition = -10;
    pipePair.name = @"hole";
//    CGFloat y = arc4random() % (NSInteger)( self.frame.size.height / 3 );
    
    SKSpriteNode* pipe1 = [SKSpriteNode spriteNodeWithTexture:_jumpBlockTexture];
    //[pipe1 setScale:2];
    pipe1.position = CGPointMake( 0, 10 );
    pipe1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(pipe1.size.width-10, pipe1.size.height - 5)];
    pipe1.physicsBody.dynamic = NO;
    
    pipe1.physicsBody.categoryBitMask = jumpBlockCategory;
    pipe1.physicsBody.contactTestBitMask = runnerCategory;
    pipe1.physicsBody.collisionBitMask = runnerCategory;
    pipe1.name = @"pipe1";
    [pipePair addChild:pipe1];
    
    [pipePair runAction:_moveAndRemovejumpBlocks];
    
    [_jumpBlocks addChild:pipePair];
}

- (void)spawnBreakBlocks
{
    int index = rand() % (_breakBlockImgNames.count - 1);
    NSLog(@"index = %d", index);
    _breakBlockTexture = [SKTexture textureWithImageNamed:_breakBlockImgNames[index]];
    _breakBlockTexture.filteringMode = SKTextureFilteringNearest;
    
    SKNode* blocksPair = [SKNode node];
    blocksPair.position = CGPointMake( self.frame.size.width + _breakBlockTexture.size.width * 2, self.size.height *0.3f - 20);
    blocksPair.zPosition = -10;
    blocksPair.name = @"breakBlock";
    //    CGFloat y = arc4random() % (NSInteger)( self.frame.size.height / 3 );
    
    SKSpriteNode* block = [SKSpriteNode spriteNodeWithTexture:_breakBlockTexture];
    [block setScale:1.5];
    block.position = CGPointMake( 0, 10 );
    block.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(block.size.width-10, block.size.height - 5)];
    block.physicsBody.dynamic = NO;
    
    block.physicsBody.categoryBitMask = breakBlockCategory;
    block.physicsBody.contactTestBitMask = runnerCategory;
    block.physicsBody.collisionBitMask = runnerCategory;
    block.name = @"break";
    [blocksPair addChild:block];
    
    [blocksPair runAction:_moveAndRemovebreakBlocks];
    
    [_breakBlocks addChild:blocksPair];
}

#pragma mark help methods

-(void)stopGame {
    
    [self showGameOverLayer];
}

- (void) showGameOverLayer
{
    [self removeAllActions];
    [self removeAllChildren];
    
    _scoresNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    _scoresNode.text = [NSString stringWithFormat:@"%d", _totalScores + _grannyScore];
    _scoresNode.fontSize = 54;
    _scoresNode.fontColor = [SKColor whiteColor];
    _scoresNode.zPosition = 7;
    _scoresNode.position = CGPointMake(self.size.width*0.7f, self.size.height*0.25f);
    
    [self addChild:_scoresNode];
    [self addChild:_gameOverLayer];
}

-(void)gameOverLayer:(GameOverLayer *)sender tapRecognizedOnButton:(GameOverLayerButtonType)gameOverLayerButtonType
{
    [sender removeFromParent];
    [_scoresNode removeFromParent];
    [self createAll];
}

@end
