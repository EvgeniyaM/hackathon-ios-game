//
//  GameViewController.m
//  WorkRunner
//
//  Created by Faust on 05.08.15.
//  Copyright (c) 2015 Mobigear. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"


@implementation GameViewController

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    //    [self IphoneModel];
    SKView * skView = (SKView *)self.view;
    
    if (!skView.scene)
    {
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        
        GameScene *scene = [GameScene sceneWithSize:skView.bounds.size];
        scene.parentController = self;
        scene.scaleMode = SKSceneScaleModeAspectFill;
        [skView presentScene:scene];
        
    }
    CGPoint origin;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        origin = CGPointMake(0.0, 0.0);
    }else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        origin = CGPointMake(0.0, 0.0);
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskLandscape;
    } else {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
