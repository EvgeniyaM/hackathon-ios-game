//
//  GameHelperLayer.h
//  RuralDuck
//
//  Created by Admin on 05.06.14.
//  Copyright (c) 2014 Diploma. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameHelperLayer : SKNode

- (id)initWithSize:(CGSize)size;

@end
